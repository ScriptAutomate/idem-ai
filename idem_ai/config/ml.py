def __init__(hub):  # noqa: N807
    # Add pop-ml's namespace to the hub
    hub.pop.sub.add(dyne_name="ml")
